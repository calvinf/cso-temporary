import _ from 'lodash';
import ContainerFluid from '../components/container-fluid';
import { format } from 'date-fns';
import fetch from 'node-fetch';
import getFilters from '../helpers/get-filters';
import getOffersUrl from '../helpers/get-offers-url';
import OffersControls from '../components/offers-controls';
import OffersTable from '../components/offers-table';
import Page from '../layout/page';
import React from 'react';
import { renderToString } from 'react-dom/server';

// Home (Index) Route
const HOME_URL = '/';

// offers page component
function OffersPage(props) {
    const { data, filters } = props;

    return (
        <Page>
            <ContainerFluid>
                <h1>Shipment Offers</h1>
                <OffersControls filters={filters} />
                <OffersTable data={data} />
            </ContainerFluid>
        </Page>
    );
}

// get the offers data from Convoy's mock API
async function getOffersData(filters) {
    const url = getOffersUrl(filters);

    let offersData;

    try {
        const offersResponse = await fetch(url.toString());
        offersData = await offersResponse.json();
    } catch (e) {
        // TODO add retry
        // or display loading failed message
        // for now, gracefully failing
        offersData = {};
    }

    return offersData;
}

export default function(app) {
    app.get(HOME_URL, async function(req, res) {
        const filters = getFilters(req);
        const offersData = await getOffersData(filters);

        // print doctype
        res.write('<!doctype html>');

        // render page
        res.write(renderToString(<OffersPage data={offersData} filters={filters} />));

        // end response
        res.end();
    });
}
