const API = 'https://convoy-frontend-homework-api.herokuapp.com';

export default {
    API,
    OFFERS: `${API}/offers`
};
