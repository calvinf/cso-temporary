import express from 'express';
import favicon from 'serve-favicon';
import home from './routes/home';
import path from 'path';
import serveStatic from 'serve-static';

const app = express();

// middleware
app.use(favicon(path.join(__dirname, 'static', 'favicon.ico')));
app.use(
    serveStatic(path.join(__dirname, 'static'), {
        dotfiles: 'ignore',
        index: false,
        maxAge: '30d'
    })
);

// register routes
home(app);

// listen
app.listen(8080);
