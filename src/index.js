// import main.js and everything beyond there can use es modules
// via esm package https://www.npmjs.com/package/esm
require = require('esm')(module);
module.exports = require('./main.js');
