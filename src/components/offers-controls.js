import React from 'react';

import Form from './form';
import PaginationControls from './pagination-controls';

function OffersControls(props) {
    const { filters } = props;
    const { offset } = filters;

    return (
        <div className="offer-controls">
            <Form filters={filters} />
            <PaginationControls offset={offset} />
        </div>
    );
}

export default OffersControls;
