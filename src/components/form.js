import React from 'react';
import _ from 'lodash';

function Form(props) {
    const { filters } = props;
    const { sort, offset, limit, order } = filters;

    const limitVal = parseInt(limit, 10);
    const offsetInput = _.isNumber(offset) ? (
        <input id="offer-offset" name="offset" type="hidden" value={offset} />
    ) : (
        ''
    );

    return (
        <form action="/" id="offer-form">
            <label className="offer-label" htmlFor="offer-sort">
                Sort
            </label>
            <select className="offer-filter" name="sort" id="offer-sort">
                <option selected={sort === 'pickupDate'} value="pickupDate">
                    Pickup Date
                </option>
                <option selected={sort === 'dropoffDate'} value="dropoffDate">
                    Dropoff Date
                </option>
                <option selected={sort === 'price'} value="price">
                    Price
                </option>
                <option selected={sort === 'origin'} value="origin">
                    Origin
                </option>
                <option selected={sort === 'destination'} value="destination">
                    Destination
                </option>
                <option selected={sort === 'miles'} value="miles">
                    Miles
                </option>
            </select>

            <label className="offer-label" htmlFor="offer-order">
                Order
            </label>
            <select className="offer-filter" name="order" id="offer-order">
                <option selected={order === 'asc'} value="asc">
                    Ascending
                </option>
                <option selected={order === 'desc'} value="desc">
                    Descending
                </option>
            </select>

            <label className="offer-label" htmlFor="offer-limit">
                Limit
            </label>
            <select className="offer-filter" name="limit" id="offer-limit">
                <option selected={limitVal === 20} value="20">
                    20
                </option>
                <option selected={limitVal === 50} value="50">
                    50
                </option>
                <option selected={limitVal === 100} value="100">
                    100
                </option>
            </select>

            {offsetInput}

            <input className="btn btn-sm btn-primary" type="submit" value="Go" id="offer-submit" />
        </form>
    );
}

export default Form;
