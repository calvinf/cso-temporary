import React from 'react';

function PaginationControls(props) {
    const { offset } = props;

    const prev =
        offset && offset > 0 ? (
            <a href="#" id="offer-prev">
                Previous
            </a>
        ) : (
            'Previous'
        );
    const next = (
        <a href="#" id="offer-next">
            Next
        </a>
    ); // TODO set boundary

    const paginationControls = (
        <>
            {prev} | {next}
        </>
    );

    return paginationControls;
}

export default PaginationControls;
