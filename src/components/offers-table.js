import React from 'react';

import _ from 'lodash';
import { format } from 'date-fns';

// data keys for parsing JSON
const keys = [
    'miles',
    'offer',
    'origin.city',
    'origin.state',
    'origin.pickup.start',
    'origin.pickup.end',
    'destination.city',
    'destination.state',
    'destination.dropoff.start',
    'destination.dropoff.end'
];

// common date format
function dateFormat(val) {
    return format(val, 'MM-DD-YYYY HH:MM');
}

// common settings for date columns
// monospace so dates are all same proportions and easier to read
const dateColSettings = {
    formatter: dateFormat,
    extraClasses: 'text-monospace'
};

// formatters for various fields
const columnSettings = {
    // align right w/ monospace so easier to tell which routes are longer
    miles: {
        extraClasses: 'text-right text-monospace'
    },

    offer: {
        formatter: val => {
            if (_.isNumber(val)) {
                // TODO update to handle multiple currencies as applicable
                // for now, assuming USD
                return val.toFixed(2);
            }

            return val;
        },
        extraClasses: 'text-right text-monospace'
    },

    // dates
    'origin.pickup.start': dateColSettings,
    'origin.pickup.end': dateColSettings,
    'destination.dropoff.start': dateColSettings,
    'destination.dropoff.end': dateColSettings
};

// generate headers from keys
const headers = keys.map((key, index) => {
    // simple method, will only work for English
    // needs updated if i18n support required
    const name = key.split('.').join(' ');

    return (
        <th scope="col" key={`col-${index}`}>
            {name}
        </th>
    );
});

// loop through mock data and return HTML table row elements
function getRows(data) {
    // loop through each entry
    return _.map(data, (entry, rowIndex) => {
        // loop through each key in order to create cells for table
        const rowData = _.map(keys, (key, index) => {
            // get the key
            let cellData = _.get(entry, key);
            let classnames;

            // apply formatting and styling as applicable
            if (columnSettings[key]) {
                if (columnSettings[key].formatter) {
                    cellData = columnSettings[key].formatter(cellData);
                }

                // extra cell styles
                if (columnSettings[key].extraClasses) {
                    classnames = columnSettings[key].extraClasses;
                }
            }

            // return the cell
            return (
                <td className={classnames} key={`${key}-${index}`}>
                    {cellData}
                </td>
            );
        });

        // return the table rows
        return (
            <tr scope="row" key={`row-${rowIndex}`}>
                {rowData}
            </tr>
        );
    });
}

// the actual offers Table
function OffersTable(props) {
    const { data } = props;

    // get the table rows elements based on the data
    const rows = getRows(data);

    return (
        <table className="table" id="offers-table">
            <thead>
                <tr>{headers}</tr>
            </thead>
            <tbody>{rows}</tbody>
        </table>
    );
}

export default OffersTable;
