const getFilters = function(req) {
    const sort = req.query.sort ? req.query.sort : 'pickupDate';
    const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0;
    const limit = req.query.limit ? parseInt(req.query.limit, 10) : 50;
    const order = req.query.order ? req.query.order : 'asc';

    return {
        sort,
        offset,
        limit,
        order
    };
};

export default getFilters;
