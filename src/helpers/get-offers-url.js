import constants from '../constants';
import URL from 'url-parse';

const getOffersUrl = function(filters) {
    // TODO validate values for params
    const { sort, offset, limit, order } = filters;

    let url = new URL(constants.OFFERS);

    let params = new URLSearchParams();
    params.append('sort', sort);
    params.append('offset', offset);
    params.append('limit', limit);
    params.append('order', order);

    url.set('query', params.toString());

    return url;
};

export default getOffersUrl;
