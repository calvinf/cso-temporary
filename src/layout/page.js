import React from 'react';
import Nav from './nav';

const Page = props => (
    <html>
        <head>
            <title>Shipment Offers</title>
            <meta charSet="utf-8" />
            <meta name="description" value="Shipment Offers" />

            <link
                rel="stylesheet"
                href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
                integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
                crossOrigin="anonymous"
            />
            <link rel="stylesheet" href="/offers.css" />
        </head>
        <body>
            <Nav />
            {props.children}
            <script src="/offers.js" async />
            <div className="footer text-center">Shipment Offers</div>
        </body>
    </html>
);

export default Page;
