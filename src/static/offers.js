// ES5 for wider browser support
// can set up Webpack + Babel for static assets to use ES2015+

// IIFE
(function() {
    var PREFIX = '#offer-';
    var FORM = PREFIX + 'form';
    var PREV = PREFIX + 'prev';
    var NEXT = PREFIX + 'next';
    var OFFSET = PREFIX + 'offset';
    var LIMIT = PREFIX + 'limit';

    var form = document.querySelector(FORM);
    var prev = document.querySelector(PREV);
    var next = document.querySelector(NEXT);
    var offsetEl = document.querySelector(OFFSET);
    var limitEl = document.querySelector(LIMIT);

    var offset = offsetEl && offsetEl.value ? parseInt(offsetEl.value, 10) : 0;
    var limit = limitEl && limitEl.value ? parseInt(limitEl.value, 10) : 20;

    function paginate(newOffset) {
        if (!offsetEl) {
            offsetEl = document.createElement('input');
            offsetEl.type = 'hidden';
            offsetEl.id = 'offer-offset';
            offsetEl.name = 'offset';

            form.appendChild(offsetEl);
        }

        offsetEl.setAttribute('value', newOffset);
        form.submit();
    }

    // if prev exists, add listener
    prev &&
        prev.addEventListener('click', function(e) {
            e.preventDefault();

            // the offset is offset minus limit or zero
            // whichever is larger so that we don't go below zero
            var newOffset = Math.max(offset - limit, 0);
            paginate(newOffset);
        });

    // if next exists, add listener
    next &&
        next.addEventListener('click', function(e) {
            e.preventDefault();

            // TODO set max limit?
            var newOffset = offset + limit;
            paginate(newOffset);
        });
})();
