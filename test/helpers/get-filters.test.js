import assert from 'assert';
import getFilters from '../../src/helpers/get-filters';

describe('filters', function() {
    describe('#getFilters()', function() {
        it('gets values from request', function() {
            const mockReq = {
                query: {
                    order: 'desc',
                    offset: '900',
                    limit: '100',
                    sort: 'destination'
                }
            };

            const filters = getFilters(mockReq);
            assert.equal(filters.order, 'desc', 'order is desc');
            assert.equal(filters.offset, 900, 'offset is 900');
            assert.equal(filters.limit, 100, 'limit is 100');
            assert.equal(filters.sort, 'destination', 'sort is destination');
        });
    });
});
