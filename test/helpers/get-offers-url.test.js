import assert from 'assert';
import getOffersUrl from '../../src/helpers/get-offers-url';

const EXPECTED_URL =
    'https://convoy-frontend-homework-api.herokuapp.com/offers?sort=destination&offset=0&limit=20&order=desc';

describe('url', function() {
    describe('#getOffersUrl()', function() {
        it('generates correct url', function() {
            const filters = {
                order: 'desc',
                sort: 'destination',
                offset: 0,
                limit: 20
            };

            const url = getOffersUrl(filters);
            assert.equal(url, EXPECTED_URL, 'not the right url');
        });
    });
});
