# Shipment Offers Application

## OS Dependencies

* Node 10.x
* Yarn (latest)

## Installing

* `yarn install`
  * install all node module dependencies

## Running

* App: `yarn start`
* Tests: `yarn test`
* Tests (with watch): `yarn test:watch`
* Prettify: `yarn prettify`

## Source

* `/src/` - entry points and global constants
  * `components` - application components for rendering HTML
  * `data` - mock data used in initial development (not currently in use by app)
  * `helpers` - utility/helper functions with specific purposes
  * `layout` - page layout
  * `routes` - top level routes, currently only `home` for the `/` path
  * `static` - static assets
* `/test/` -- all tests live here (mirrors `src` structure, tests have `*.test.js` naming)

## Nodemon, Babel, and ESM

By using `nodemon`, able to refresh during development easily.  `babel-node` enables transpiling JSX on the fly (in production, will need to integrate build step to pre-compile JSX).  `esm` module allows ES Modules usage in server side node.js code (node.js has really messy support for this behind a flag and doesn't follow same approach as browser).

## App

The app is built using Express and a few Express middlewares (for static assets).  We're doing server-side rendering with React.  On the client side, we have minimal dependencies -- we're using Bootstrap styling (but not any of their JS or JS dependencies).  Most app interaction is via standard HTML -- the only client-side JS code is for updating the `offset` value in the form so `next` and `prev` pagination will work properly.

## Unfinished pieces

For productionalization, a few key changes would be required.

* Remove `babel-node` dependencies for prod code by pre-compiling JSX via `babel`.
* Integrate Webpack or other bundler to produce optimized CSS & JS builds for client-side code.
* Use nginx to proxy requests to the node server.
* Add additional type checking to avoid unexpected inputs.
* Integrate client-side async module loader so we can start loading script dependencies earlier and only execute when all dependencies are available.
* Add fallbacks for when API fails -- error message in page, potentially integrate retry logic.